data.coop-website
=================

[![Build Status](https://drone.data.coop/api/badges/data.coop/website/status.svg)](https://drone.data.coop/data.coop/website)

This is a [Hugo](https://gohugo.io/) project.

Running with Docker
-------------------

In `docker-compose.yml`, we have specified a `serve` target which you can run locally like this:

```bash
docker compose run --rm -u `id -u` --service-ports serve
```

Running without Docker
----------------------

Go to [Hugo Github release](https://github.com/gohugoio/hugo/releases)
and fetch the latest package for **hugo\_extended** for your system.

We want to align with the latest version always. If it doesn't work,
file an issue!

Example recipe

```bash
# Fetch .deb from Github
wget https://github.com/gohugoio/hugo/releases/download/v0.80.0/hugo_extended_X.Y.Z_Linux-64bit.deb -O hugo_extended.deb

# Install package
sudo dpkg -i hugo_extended.deb

# Clone repo
git clone https://git.data.coop/data.coop/website.git data.coop-website

# Go to website
cd data.coop-website

# Run development server
hugo server
```

Deploying the site
------------------

Simply pushing to `main` in our main repo at
<https://git.data.coop/data.coop/website/> will trigger a
build-and-deploy of the website.

Multilingual notes
------------------

Our website is made multi-lingual using the following 3 structures:

1.  The folder `i18n/` contains translations of strings used in the
    theme.
2.  Each content article and news text has a language version such
    `content/<slug>.en.md`
3.  The `config.yaml` contains settings specific to each language, such
    as navigation.

