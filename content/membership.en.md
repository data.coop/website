---
title: Become a member
---

[data.coop]({{< ref "/" >}}) is a cooperative established as a democratic association.
The members decide on the association's operations and how data should be governed.
Governance resembles any classic association: We have legal statutes and formal general assemblies.

You need to read our [statutes]({{< relref "/rights.md" >}}) and our
[Acceptable Usage Policy (AUP)](https://git.data.coop/data.coop/dokumenter/src/branch/main/Acceptable%20Usage%20Policy.md)
before becoming a member.
If you have questions, you can find us on [Matrix or IRC]({{< ref "/about.md" >}}) or [send an email to the board](mailto:board@data.coop).

*English speakers:* We would love to have you as a member.
Many parts of this organization are already in English,
but our legal documents are not (yet).
Please write us if you have any questions.

## Price

Our membership period runs from one general assembly to the next => July 1st until June 30th.

A membership is comprised of a contingent to the association and a service fee. Payments follow the membership period.

* Ordinary membershio: **600 kr / år**
  * ...of which 150 kr is contingent
  * ...and 450 kr are service fee (incl. VAT)
* Reduced membership: **100 kr / år**
  * ...of which 50 kr is contingent
  * ...and 50 kr is service fee (incl. VAT)

The reduced membership is offered to students and unemployed members.

## How to become a member

We are developing a sign-up form.

Currently, you sign up by writing members@data.coop with the following info:

* That you have read and agree to our [statutes]({{< ref "/rights.md" >}}) and [Acceptable Usage Policy (AUP)](https://git.data.coop/data.coop/dokumenter/src/branch/main/Acceptable%20Usage%20Policy.md).
* A desired primary username
* The email address to connect to your account

After this, we can send you an invite and payment link.
