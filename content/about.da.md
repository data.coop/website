---
title: "Om os"
---

**[data.coop]({{< ref "/" >}})** er en forening og et kooperativ.

Vi — medlemmerne i kooperativet — ejer vores egne data.

Overordnet betyder dette:

 * At vi ejer vores egen hardware
 * At vi kun bruger open source software

Foreningen råder over 2 rack-servere, hvoraf den ene pt. er i brug.

Du kan finde os på:

 * Matrix: [#data.coop:data.coop](https://matrix.to/#/#data.coop:data.coop)
 * IRC (Libera.chat): #data.coop
 * Vores [Forgejo server](https://git.data.coop/data.coop/)
 * På Fødiverset/Mastodon: <a rel="me" href="https://social.data.coop/@datacoop">@datacoop@data.coop</a>
