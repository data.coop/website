---
title: Vedtægter for datafællesskabet data.coop
---

## § 1. Navn og tilhørsforhold
Foreningens navn er: data.coop

Foreningens hjemsted er Københavns Kommune, men primært internettet.

## § 2. Formål

Foreningens hovedformål er at stille digital infrastruktur til rådighed
for sine medlemmer, på en måde hvor foreningens kerneprincipper --
privatlivsbeskyttelse, kryptering, decentralisering og zero-knowledge for
foreningen som tjenesteudbyder -- er i fokus.

### § 2.1 Sekundære formål

 * Foreningen arbejder med at advokere for sine kerneprincipper.
 * Foreningen hjælper folk til at at agere på nettet på forsvarlig vis.
 * Foreningen samarbejder med andre datafællesskaber og/eller hjælper andre
   i gang med lignende foreninger.
 * Foreningen kan stille sine tjenester til rådighed for almennyttige foreninger,
   græsrodsorganisationer og andre, der uafhængigt af foreningen, og uden at have
   profit for øje, arbejder med formål der overlapper med foreningens egne formål
   og kerneprincipper.

## § 3. Organisation
Foreningens overordnede ledelse er generalforsamlingen, som består af medlemmer med gyldigt medlemsbevis.
Generalforsamlingen vælger:

- En bestyrelse på 5 personer
- 2 suppleanter for bestyrelsen
- 1 revisor
- Evt. 1 suppleant for revisor

Valg til bestyrelsen sker for en periode på 2 år, således at mindst 2 personer
er på valg hvert år. Revisor og suppleanter er på valg hvert  år.

## § 4. Generalforsamlingen
Bestyrelsen indkalder generalforsamlingen med mindst 14 dages varsel via
foreningens hjemmeside og mailinglister. Ordinær generalforsamling afholdes
hvert år inden udgangen af september måned. Dagsorden for ordinær
generalforsamling skal indeholde følgende punkter:

1. Formalia
   1. Valg af dirigent.
   2. Generalforsamlingens lovlighed (er indkaldelsen sket rettidigt). 
   3. Valg af referent. 
2. Bestyrelsens beretning.
3. Fremlæggelse og godkendelse af regnskab, budget og kontingent.
4. Indkomne forslag.
5. Valg (Jf. § 3)
6. Eventuelt

### § 4.1. Afholdelse af generalforsamlinger og bestyrelsesmøder
Generalforsamlinger og bestyrelsesmøder kan afholdes på internettet.

### § 4.2. Indkomne forslag og vedtægtsændringer 
Forslag og vedtægtsændringer skal være modtaget af bestyrelsen senest en uge før generalforsamlingen og fremlægges for medlemmerne senest 4 dage før generalforsamlingen.

### § 4.3. Vedtægtsændringer
Vedtægtsændringer kræver et flertal på 3/4 af generalforsamlingens fremmødte medlemmer.

### § 4.4. Forslag
Forslag kræver et simpelt flertal af generalforsamlingens fremmødte medlemmer.


## § 5. Foreningens bestyrelse
Foreningens daglige ledelse forestås af bestyrelsen (Jf. § 3).  Bestyrelsen
konstituerer sig selv med forperson, næstforperson og kasserer.  Bestyrelsen
uddelegerer den daglige drift til et forretningsudvalg, som  består af forperson,
næstforperson og kasserer.

Bestyrelsen udarbejder regnskab og budget.

Bestyrelsen fastsætter selv sin forretningsorden.

Bestyrelsen kan sammensætte samarbejdsgrupper af medlemmer, der kan fungerer
som idéudviklingsforum for foreningen.

### § 5.1 Tegningsret

Tegningsret for foreningen har forpersonen og kassereren, dog ved køb, salg eller
pantsætning af fast ejendom, indgåelse af  driftsaftaler samt ved optagelse af
lån, af den samlede bestyrelse.

Den samlede bestyrelse kan meddele prokura til individuelle medlemmer af bestyrelsen.

## § 6. Medlemskab
Som medlem kan enhver fysisk person optages, som har interesse i at støtte
foreningens formål. Medlemskab er bindende for et år ad gangen.

Bestyrelsen kan ekskludere medlemmer fra foreningen, hvis disse har handlet i
uoverenstemmelse med Acceptable Use Policy (AUP).

Medlemskab er fortløbende fra betaling af kontingent frem til overstået ordinær generalforsamling.

## § 7. Administratorer
Driften af foreningens services forestås af et Administratorhold. Kun medlemmer
af foreningen kan bestride hvervet som administrator.

Administratorer er de eneste, der har administrativ adgang til foreningens
servere og tjenester.

### § 7.1 Udpegning af administratorer
Bestyrelsen udpeger administratorer.

Udpegning af administratorer skal annonceres til foreningens medlemmer.

### § 7.2 Mistillid til administratorer
Hvis der er mistillid til en eller flere administratorer skal dette behandles
på en ekstraordinær generalforsamling.

Mistillid til administratorer skal meldes til bestyrelsen. Bestyrelsen kan midlertidigt fratage en administrator vedkommendes administrationsrettigheder, indtil mistillidserklæringen mod vedkommende er behandlet.

## § 8. Moderationspolitik
Foreningen vedtager en moderationspolitik for foreningens sociale tjenester på generalforsamlingen. Moderationspolitikken håndhæves af et moderatorhold.

### § 8.1 Udpegning af moderatorer
Bestyrelsen udpeger moderatorer.

Udpegning af moderatorer skal annonceres til foreningens medlemmer.

Kun medlemmer af foreningen kan bestride hvervet som moderator. Moderatorer skal så vidt muligt findes udenfor bestyrelsen, dog skal mindst én moderator samtidig være bestyrelsesmedlem.

### § 8.2 Mistillid til moderatorer
Hvis der er mistillid til en eller flere moderatorer skal dette behandles
på en ekstraordinær generalforsamling.

Mistillid til moderatorer skal meldes til bestyrelsen. Bestyrelsen kan midlertidigt fratage en moderator vedkommendes moderatorrettigheder, indtil mistillidserklæringen mod vedkommende er behandlet.

## § 9. Kontingent/finansiering
De årlige kontingenter fastsættes af generalforsamlingen.

Foreningen kan herudover finansieres ved sponsor- og  annoncestøtte samt bidrag
fra offentlige/private virksomheder, fonde, øvrige foreninger/institutioner og
private personer, så længe det ikke stiller krav til foreningens dispositioner.

## § 10. Medlemsydelse

Foreningen stiller tjenester til rådighed for sine medlemmer mod betaling af medlemsydelse.

Medlemsydelsen defineres og prissættes af bestyrelsen.

### § 10.1 Leverings- og betalingsbetingelser

Bestyrelsen fastsætter leverings- og betalingsbetingelser, såsom betalingsperioder, frister og refusion.

Bestyrelsen forudsætter sig ret til at pålægge gebyrer eller lukke for tjenester ved udebleven betaling.

### § 10.2 Ændring af medlemsydelser og betalingsbetingelser

Ændring af ydelser og betalingsbetingelser skal annonceres til foreningens medlemmer mindst 30 dage inden ændringen træder i kraft.

Tilføjelser af flere tjenester eller ændringer på konfigurationen af tjenester anses IKKE som ændringer, der kræver varslinger.

### § 10.3 Indsigelse mod ændringer

Hvis et eller flere medlemmer er uenige i ændringer af medlemsydelsen eller betalingsbetingelser, kan de indsende en skriftlig indsigelse til bestyrelsen.

Indsigelsen skal være bestyrelsen i hænde senest 14 dage før ændringen træder i kraft.

Hvis bestyrelsen modtager indsigelser fra mindst 1/3 af foreningens medlemmer, skal bestyrelsen indkalde til en ekstraordinær generalforsamling, hvor ændringen skal fremsættes som forslag og godkendes af generalforsamlingen.

## § 11. Ekstraordinær generalforsamling
Indkaldelse sker, hvis et flertal af bestyrelsen ønsker det.

Indkaldelse sker, hvis 1/3 af medlemmerne ønsker det.

Indkaldelse sker under samme betingelser, som anført i §4.

Dagsorden skal motiveres.

## § 12. Regnskab
Regnskabsåret for Foreningen er kalenderåret.

## § 13. Opløsning
Opløsning af foreningen kræver et flertal på ¾ af generalforsamlingens eller
den ekstraordinære generalforsamlings fremmødte medlemmer.

Opløsningen skal herefter godkendes på en efterfølgende ekstraordinær
generalforsamling.

Ved opløsning af foreningen skal foreningens midler overdrages til European
Digital Rights (EDRi) og Free Software Foundation Europe (FSFE).

Foreningen data.coop er stiftet den 24 juni 2014.
