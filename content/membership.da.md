---
title: Bliv medlem
---

[data.coop]({{< ref "/" >}}) er en demokratisk forening. Medlemmer bestemmer
over foreningens generelle drift og hvordan data skal forvaltes. Dette sker som i en helt klassisk forening i henhold til vores vedtægter på den årlige
generalforsamling.

Du bør læse vores [vedtægter]({{< ref "/rights.md" >}}) og især vores [Acceptable Usage Policy (AUP)](https://git.data.coop/data.coop/dokumenter/src/branch/main/Acceptable%20Usage%20Policy.md) inden du melder dig ind. Har du spørgsmål, kan du finde os på [Matrix og IRC]({{< ref "/about.md" >}}) eller [sende en e-mail til bestyrelsen](mailto:board@data.coop).

## Medlemspris

Vores kontingentår løber fra og med 1. juli til og med 30. juni.

Medlemskab består af et kontingent til foreningen, samt en medlemsydelse for brug af tjenesterne. Betalinger følger kontingentåret. Dvs. medlemskab er fortløbende fra betaling af kontingent frem til overstået ordinær generalforsamling. Medlemsydelsen er ligeledes 1 år forudbetalt og følger også kontingentåret, i indmeldelsesåret er dog kun indregnet tilbageværende måneder til og med juni.

* Almindeligt medlem: **600 kr / år**
  * Heraf 150 kr kontingent
  * ...og 450 kr medlemsydelse (inkl. moms)
* Nedsat kontingent: **100 kr / år**
  * Heraf 50 kr kontingent
  * ...og 50 kr medlemsydelse (inkl. moms)

Nedsat kontingent tilbydes til studerende og arbejdsløse.

## Sådan melder du dig ind

En betalingsløsning er under udvikling.

Du melder dig ind ved at skrive til members@data.coop med følgende information:

* At du har læst og er enig i vores [vedtægter]({{< ref "/rights.md" >}}) og [Acceptable Usage Policy (AUP)](https://git.data.coop/data.coop/dokumenter/src/branch/main/Acceptable%20Usage%20Policy.md).
* Ønsket primært brugernavn
* Din email-adresse (som bliver tilknyttet services)

Herefter kan vi sende dig en invitation og et betalingslink.
