---
title: Legal statutes of data.coop
---

> This translation is provided solely for the purpose of guidance. The [Danish version](/rights/) remains the official and legally binding version.

## § 1. Name and location
The name of the association is: data.coop

The association is based in the city of Copenhagen but (its activities are) primarily online.

## § 2. Aim

The main aims of the association is to provide digital infrastructure to its members, while keeping the core principles of the association — privacy, encryption, decentralization and zero knowledge — in focus.

### § 2.1 Secondary aims

* The association works to promote its core principles.
* The association helps people act responsibly online.
* The association collaborates with other data cooperatives and/or helps other start up similar initiatives.
* The association may provide its services to other non-profits, grassroots movements and similar, working independently of the association with goals that match the aims and core principles of the association.

## § 3. Governance
Overriding power of the association lies with the General Assembly, consisting of members with a valid proof of membership.

The General Assembly elects:

- A board of 5 members
- 2 alternate board members
- 1 financial auditor
- Optionally, 1 alternate financial auditor

Board members are elected for a period of 2 years, so that at least 2 members are up for election every year. The auditor and alternates are up for election every year.

## § 4. The General Assembly (GA)
The board calls for meetings of the GA no less than 14 days prior through the website of the association and mailing lists. The GA meets every year before the end of September. The agenda of the annual meeting of the GA must include the following items:

1. Formalities
   1. Election of a meeting chair and notetaker
   2. Legality of the meeting (was the meeting called in time)
   3. Election of a notetaker
2. Report from the board
3. Presentation and approval of accounts, budget and membership fee
4. Incoming motions
5. Elections (as per §3)
6. Any other business

### § 4.1. Holding of meetings of the GA and the board
Meetings of the GA and the board may take place online.

### § 4.2. Incoming motions and statute amendments

Motions and statute amendments must be received by the board no less than a week before the meeting of the GA and presented to members no less than 4 days before the the meeting of the GA.

### § 4.3. Statute amendments

Passing of statute amendments requires a three-quarters majority of members present at the meeting of the GA.

### § 4.4. Motions
Passing of motions requires a simple majority of members present at the meeting of the GA.

## § 5. The board of the association
The executive power of the association lies with the board (as per §3). The board itself elects its officers including a chair, a vice chair and a treasurer. The board delegates handling of daily business of the association to an executive committee consisting of the chair, vice chair and treasurer.

The board is in charge of drafting the accounts and the budget.

The board itself determines its own rules of procedure.

The board may create ad-hoc working groups consisting of members, functioning as fora for developing ideas for the association.

### §5.1 Legal authority

The chair and treasurer have legal authority, but when it comes to purchasing, selling, or mortgaging real estate, making service agreements, or taking loans, the board as a whole must agree to bind the association.

The board as a whole may assign power of attorney to individual members of the board.

## § 6. Membership
Any physical person who supports the aims of the association may become a member. Membership is binding for one year at a time.

The board has the authority to remove members from the association if they act in violation of the Acceptable Use Policy (AUP).

Membership is continuous from the payment of dues until the conclusion of the annual meeting of the GA.

## § 7. Administrators
The services offered by the associated are maintained by a team of administrators. Only members of the association can assume the role of administrator.

Only administrators hold administrative access to the servers and services of the association.

### § 7.1 Appointment of administrators
The board appoints administrators.

The appointment of administrators must be announced to the members of the association.

### § 7.2 Mistrust of administrators
Mistrust towards one or more administrators must be addressed at an extraordinary meeting of the GA.

Mistrust towards administrators must be reported to the board. The board has the authority to temporarily suspend an administrator's administrative rights until the declaration of mistrust against them is addressed

## § 8. Moderation policy
The association maintains a moderation policy for its social services, approved by the GA. The moderation policy is enforced by a team of moderators.

### § 8.1 Appointment of moderators
The board appoints moderators.

The appointment of moderators must be announced to the members of the association.

Only members of the association can hold the position of moderator. Ideally, moderators should not be board members, however, at least one moderator must also be a board member.

 ### § 8.2 Mistrust of moderators
Mistrust towards one or more moderators must be addressed at an extraordinary meeting of the GA.

Mistrust towards moderators must be reported to the board. The board has the authority to temporarily suspend a moderator's moderator rights until the declaration of mistrust against them is addressed

## § 9. Membership fees/funding
Annual membership fees are decided by the GA.

Additionally, the association can receive funding through sponsorships, advertising support, as well as contributions from public/private companies, foundations, other associations/institutions, and private individuals, as long as it does not impose conditions on the association's decisions.

## § 10. Service fees

The association provides services to its members who pay a service fee.

The service fee is defined and determined by the board.

### § 10.1 Terms of delivery and payment

The board determines the terms of delivery and payment, such as terms, payment times and refunds.

The board reserves the right to charge late fees or suspend services in case of lacking payments.

### § 10.2 Change of service fees and payment terms

Changes to service fees or payment terms must be announced to the members of the association no later than 30 days before taking effect.

The addition of services or changes to service configurations are NOT considered changes requiring notice.

### § 10.3 Disputing changes

If one or more members disagree with changes in service fees or payment terms, they may file a dispute in writing to the board.

The dispute must be received by the board no later than 14 days before the change would take effect.

If the board receives disputes from at least 1/3 of the members of the association, the board must call for an extraordinary GA, at which the change must be presented as a motion to be approved by the GA.

## § 11. Extraordinary General Assembly (GA)
Extraordinary GAs may be called if desired by a majority of the board.

Extraordinary GAs may be called if desired by one-third of the members.

Extraordinary GAs are called following the same terms as in §4.

The agenda must be substantiated.

## § 12. Accounts
The association's financial year is the calendar year.

## § 13. Dissolution
Dissolution of the association requires a majority vote of three-quarters of members present at the GA or extraordinary GA.

The dissolution must then be approved at a subsequent extraordinary GA.

In the event of the dissolution of the association, the association's assets shall be transferred to European Digital Rights (EDRi) and Free Software Foundation Europe (FSFE)

The association data.coop was founded on June 24, 2014.