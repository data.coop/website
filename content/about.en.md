---
title: "About"
---

**[data.coop]({{< ref "/" >}})** is an association and a cooperative.

The community consists of members around the Copenhagen-area and is open to inquiries from abroad to start sister infrastructures based on the same principles.
If you are interested in becoming a member of data.coop but you are not able to participate in physical meetings in Copenhagen, we encourage that you get in touch with us about a vision you have for your community, rather than signing up as a member.

We — the members of the cooperative — own our own data.

In practice, this means that:

 * We own our own hardware.
 * We only use Open Source Software.

The cooperative owns 2 rack servers, of which one is in use and one is spare.

You can find us here:

 * Matrix: [#data.coop:data.coop](https://matrix.to/#/#data.coop:data.coop)
 * IRC (Libera.chat): #data.coop
 * Our [Forgejo server](https://git.data.coop/data.coop/)
 * In the Fediverse/Mastodon: <a rel="me" href="https://social.data.coop/@datacoop">@datacoop@data.coop</a>
