---
layout: page
title: PrivateBin
service_url: https://paste.data.coop
service_badges:
  stability: positive
  connection_security: positive
  connection_anonymous: partial
  storage_encrypted: positive
  zero_knowledge: positive
  storage_backup: positive
  logging: negative
---

Del en simpel hemmelighed gennem et unikt og hemmeligt link.
Du kan tilsætte password og sørge for, at hemmeligheden slettes automatisk.<!--more-->

Denne service er åben for alle og kræver ikke login.
