---
layout: page
title: Rallly
service_url: https://when.data.coop
service_badges:
  stability: positive
  connection_security: positive
  connection_anonymous: partial
  storage_encrypted: negative
  zero_knowledge: negative
  storage_backup: positive
  logging: negative
---

Rally makes it easy to find a date for an event with your friends, family or any other group of people.
It's a lot like Doodle, but better!
