---
layout: page
title: Tjenester
---

Visse tjenester **data.coop** driver er kun for medlemmer, andre er åbne for alle.

Alle tjenester er benævnt med [badges]({{< ref "/services/#badges" >}}), der deklarerer i hvor høj grad den
enkelte tjeneste lever op til [kerneprincipperne defineret i formålsparagraffen
i vores vedtægter]({{< ref "/rights.md#-2-formål" >}}).

Vi har skrevet en definition af [hvad de forskellige badges dækker over]({{< ref "/services/#badges" >}}).

Klik på hver tjenestes navn for at se detaljer:
