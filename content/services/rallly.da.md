---
layout: page
title: Rallly
service_url: https://when.data.coop
service_badges:
  stability: positive
  connection_security: positive
  connection_anonymous: partial
  storage_encrypted: negative
  zero_knowledge: negative
  storage_backup: positive
  logging: negative
---

Rallly gør det nemt at finde en dato med dine venner/familie/gruppe.
Det minder om Doodle, men det er bedre!
