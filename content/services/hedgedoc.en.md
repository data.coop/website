---
layout: page
title: HedgeDoc 
service_url: https://pad.data.coop
service_badges:
  stability: positive
  connection_security: positive
  connection_anonymous: partial
  storage_encrypted: negative
  zero_knowledge: partial
  storage_backup: positive
  logging: negative
---

Take random notes or write full documents in Markdown: HedgeDoc can be used for many things.<!--more-->

Most people use HedgeDoc for quickly making notes are bootstrapping documents.
You can collaborate in real-time with others.
And the results are pretty!

There are also several extensions, for instance for Mermaid diagrams.

Read more on the [HedgeDoc website](https://hedgedoc.org/).
