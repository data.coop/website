---
layout: page
title: Passit
service_url: https://passit.data.coop
service_badges:
  stability: positive
  connection_security: positive
  connection_anonymous: positive 
  storage_encrypted: positive
  zero_knowledge: partial
  storage_backup: positive
  logging: positive
---

Hjælper dig med at huske stærke, unikke kodeord til alle de sites og apps du benytter dig af.
