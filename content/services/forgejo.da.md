---
layout: page
title: Forgejo
service_url: https://git.data.coop
service_badges:
  stability: positive
  secure_connection: positive
  anonymity: partial
  encrypted_storage: negative
  zero_knowledge: negative
  backup: positive
  logging: negative
---

Skriver du kode eller anden tekst som du gerne vil holde under versionsstyring,
kan du gemme det på denne fantastiske Git-platform.<!--more-->

Platformen er åben for alle data.coops medlemmer,
og du kan forespørge konti til ikke-medlemmer.
