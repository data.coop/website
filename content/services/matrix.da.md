---
layout: page
title: Matrix 
service_badges:
  stability: positive
  connection_security: positive
  connection_anonymous: negative
  storage_encrypted: positive
  zero_knowledge: partial
  storage_backup: positive
  logging: negative
---

Matrix er en chat-server, som kan rigtig mange ting. Den kan bruges som alternativ til f.eks. Slack, Discord, IRC og meget andet.
Vi har også vores egen hostede version af reference-klienten Element på [element.data.coop](https://element.data.coop/).<!--more-->

Du kan bruge Matrix til at kommunikere med andre på hele Matrix-netværket,
dvs. ikke bare andre data.coop-medlemmer.

Matrix er en kæmpestor platform, som muliggør en lang række interaktionsformer.
Bl.a. har flere konferencer oprettet rum til live videostream og chat.
