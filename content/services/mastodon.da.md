---
layout: page
title: Mastodon
service_url: https://social.data.coop
service_badges:
  stability: positive
  connection_security: positive
  connection_anonymous: negative
  storage_encrypted: negative
  zero_knowledge: partial
  storage_backup: positive
  logging: negative
---

Del kattebilleder og memes og følg med i hvad andre spændende mennesker foretager sig.
