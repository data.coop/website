---
layout: page
title: Passit
service_url: https://passit.data.coop
service_badges:
  stability: positive
  connection_security: positive
  connection_anonymous: positive 
  storage_encrypted: positive
  zero_knowledge: partial
  storage_backup: positive
  logging: positive
---

Helps you remember your strong, unique passwords for websites and applications.
