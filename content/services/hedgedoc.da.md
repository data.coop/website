---
layout: page
title: HedgeDoc 
service_url: https://pad.data.coop
service_badges:
  stability: positive
  connection_security: positive
  connection_anonymous: partial
  storage_encrypted: negative
  zero_knowledge: partial
  storage_backup: positive
  logging: negative
---

Tag tilfældige noter eller skriv dokumenter i Markdown: HedgeDoc kan bruges til mange ting.<!--more-->

De fleste bruger HedgeDoc til hurtigt at tage noter eller starte dokumenter.
Man kan skrive samtidig.
Og resultatet er flot.

Der findes også flere udvidelser, bl.a. Mermaid-diagrammer. Læs mere på [HedgeDoc's hjemmeside](https://hedgedoc.org/).
