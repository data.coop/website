---
layout: page
title: Nextcloud
service_url: https://cloud.data.coop
service_badges:
  stability: positive
  connection_security: positive
  connection_anonymous: partial
  storage_encrypted: negative
  zero_knowledge: negative
  storage_backup: positive
  logging: negative
---

Store your files, calendar, contacts and many other things in the local cloud.
Access from all your devices.
