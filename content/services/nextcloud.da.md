---
layout: page
title: Nextcloud
service_url: https://cloud.data.coop
service_badges:
  stability: positive
  connection_security: positive
  connection_anonymous: partial
  storage_encrypted: negative
  zero_knowledge: negative
  storage_backup: positive
  logging: negative
---

Gem dine filer, din kalender og dine kontakter i skyen og tilgå alt fra alle dine enheder.
