---
layout: page
title: Forgejo
service_url: https://git.data.coop
service_badges:
  stability: positive
  secure_connection: positive
  anonymity: partial
  encrypted_storage: negative
  zero_knowledge: negative
  backup: positive
  logging: negative
---

If you write code or other types of texts that need revisioning,
you can use this fantastic Git platform<!--more-->

The platform is open for all data.coop members,
and you can ask for accounts for non-members.
