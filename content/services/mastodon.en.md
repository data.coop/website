---
layout: page
title: Mastodon
service_url: https://social.data.coop
service_badges:
  stability: positive
  connection_security: positive
  connection_anonymous: negative
  storage_encrypted: negative
  zero_knowledge: partial
  storage_backup: positive
  logging: negative
---

Share cat photos and memes and see what other interesting people are doing.
