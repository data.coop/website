---
layout: page
title: Matrix 
service_badges:
  stability: positive
  connection_security: positive
  connection_anonymous: negative
  storage_encrypted: positive
  zero_knowledge: partial
  storage_backup: positive
  logging: negative
---

Matrix is a chat server that's capable of a lot of things.
It can be used as an alternative for Slack, Discord, IRC and lots more.
We have our own hosted version of its reference client Element available at [element.data.coop](https://element.data.coop/).<!--more-->

You can use Matrix for communicating with the entire Matrix network,
not just data.coop members.

Matrix is a gigantic platform which enables a wide range of communication types.
It supports channels, live videos and chat.
