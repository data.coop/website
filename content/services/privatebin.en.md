---
layout: page
title: PrivateBin
service_url: https://paste.data.coop
service_badges:
  stability: positive
  connection_security: positive
  connection_anonymous: partial
  storage_encrypted: positive
  zero_knowledge: positive
  storage_backup: positive
  logging: negative
---

Share a secret through a unique and secret link.
You can add a password and make the secret expire and delete itself.<!--more-->

This service is open and does not require a login.
