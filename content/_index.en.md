---
title: Welcome to data.coop
---

<div class="message">
  <p>2025-01-12: data.coop blocks threads.net: <a href="{{< ref "/blog/defederating-meta.en.md" >}}">Read the decision »</a></p>
</div>

data.coop is a cooperative, which owns and operates a digital infrastructure for its members. Our fundamental vision is to protect our members' data.
The community consists of members around the Copenhagen-area and is open to inquiries from abroad to start sister infrastructures based on the same principles.

Our core principles are:

<dl class="principles-list">
  <dt>
    <img src="/static/img/icons/no-surveillance.svg" />
    Privacy
  </dt>
  <dd>
    We protect member data together.
    We do not share them for profit.
    Data is always encrypted in transit.
  </dd>
  <dt>
    <img src="/static/img/icons/encrypted.svg" />
    Encryption
  </dt>
  <dd>
    We provide secure services that are thoroughly declared.
  </dd>
  <dt>
    <img src="/static/img/icons/decentralised.svg" />
    Decentralization
  </dt>
  <dd>
    Our services exist together with other decentralized counterparts.
  </dd>
  <dt>
    <img src="/static/img/icons/zero-knowledge.svg" />
    Zero knowledge
  </dt>
  <dd>
    Whenever possible, we ensure that a system administrator does not have technical ability to access member data.
  </dd>
</dl>

From these core principles, we will continue to develop services for members.
The idea is that we trust each other more than any Big Tech operators, such as Google, Microsoft or Facebook.

