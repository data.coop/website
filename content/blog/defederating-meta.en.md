---
title: Decision to defederate from Meta-owned servers
summary: The data.coop board agrees that there are compelling arguments for blocking Meta's ActivityPub platforms, initially Threads.net.
date: 2025-01-12
params:
  author: bestyrelsen
---
*Adopted by the board on January 12, 2025*

*This decision has been added to the protocol of [data.coop's Moderation on the Fediverse (Danish)](https://git.data.coop/data.coop/dokumenter/src/branch/main/Fediverse%20Moderation.md)*

## Background

Since Meta announced that Threads.net would be connected to The Fediverse through ActivityPub, it has been requested by several members in the association that data.coop takes a general position on whether we will continue to allow our server(s) (currently social.data.coop) to exchange data with Meta/Threads, or whether we should block the exchange. Due to extensive questions and concerns about Meta's moderation policy, we have taken a closer look at the matter, which has received renewed motivation in early 2025, when Meta decided to:

- Drop their current moderation and mention the X platform (formerly Twitter) as a role model.

- Loosen the rules on hate speech and thus move in a direction (even further) away from our own rules and AUP.

## Decision Criteria

With reference to our rule "that there is a serious mistrust of the owner's ability to moderate content in accordance with our AUP or moderation policy":

* Recent statements from Mark Zuckerberg about the future of moderation on Meta's platforms in 2025 [1]
* Extensive neglect of accountability for genocide and human rights violations on Meta's platforms, as described by Amnesty in 2023 [2] [3] [4]
* Deliberate neglect of accountability for/complicity in state actor manipulation and extensive disinformation campaigns, as documented by multiple whistleblowers [5] [6] [7]
* Meta's censorship of political content, where we perceive Meta potentially willing to censor content shared by data.coop members, as described by Human Rights Watch in 2023 [8]

With reference to "That there is a conflict of interest between the owner and data.coop":

* That Meta generally works for a centralized, commercial use and collection of user data in violation of data.coop's purpose and principles.

With reference to "That the owner's activities constitute an existential threat to one or more of data.coop's services":

* There is reasonable suspicion that Threads participates in the Fediverse either to lure users to their platforms or to constitute a dominant power factor. Several members have expressed concern about "embrace, extend, extinguish"[9] scenarios.

With reference to "That there is suspicion that the owner will, without consent, use the members' ActivityPub data, collected through the federation or otherwise, in a way that requires consent."

* Current AI technologies are trained without consent on open content. Because the ActivityPub protocol does not allow for a clear definition of consent regarding the use of data, and because we already know that AIs are trained in violation of applicable copyright law, we also see it as likely that data from data.coop's members can be transferred via ActivityPub to Threads and thus, for example, used for AI training or other privacy-infringing profiling of members.

## Decision

The board agrees that overall there are convincing arguments to block Meta's ActivityPub platforms, initially Threads.net. Some arguments are enough in themselves, and together they form a clear picture that we do not want:

* Our members' data exchanged with Meta/Threads.

* Content from Meta/Threads exchanged via our servers.

***We therefore decide to block all ActivityPub-based exchange of data with Meta and Threads.net, including other potential, future ActivityPub-based services from Meta.***

## Sources

[1] https://www.dr.dk/nyheder/viden/teknologi/i-2008-var-zuckerberg-en-stor-undskyldning-det-er-han-langtfra-i-dag

[2] https://www.amnesty.org/en/latest/news/2023/08/myanmar-time-for-meta-to-pay-reparations-to-rohingya-for-role-in-ethnic-cleansing/

[3] https://www.amnesty.org/en/latest/news/2023/10/meta-failure-contributed-to-abuses-against-tigray-ethiopia/

[4] https://erinkissane.com/meta-in-myanmar-full-series

[5] https://www.theguardian.com/technology/2021/apr/12/facebook-fake-engagement-whistleblower-sophie-zhang

[6] https://en.wikipedia.org/wiki/Frances_Haugen

[7] https://en.wikipedia.org/wiki/Facebook%E2%80%93Cambridge_Analytica_data_scandal

[8] https://www.hrw.org/report/2023/12/21/metas-broken-promises/systemic-censorship-palestine-content-instagram-and

[9] https://en.wikipedia.org/wiki/Embrace,_extend,_and_extinguish
