---
title: Beslutning ang. (de)federering med Meta-ejede servere
summary: Bestyrelsen er enige om, at der samlet set er gode nok argumenter for at blokere for Metas ActivityPub-platforme, i første omgang konkret Threads.net.
date: 2025-01-12
params:
  author: bestyrelsen
categories:
- Admins
tags:
- board
- fediverse
- moderation
- policy
---

*Vedtaget af bestyrelsen 12. januar 2025*

*Denne beslutning er indsat i protokollen for [data.coop's Moderation på Fødiverset](https://git.data.coop/data.coop/dokumenter/src/branch/main/Fediverse%20Moderation.md)*

## Baggrund

Det har – siden Meta annoncerede at Threads.net skulle forbindes til "Fødiverset" gennem ActivityPub – været ønsket fra flere i foreningen, at data.coop tager generel stilling til, om vi fortsat vil tillade, at vores server(e) (p.t. social.data.coop) udveksler data med Meta/Threads, eller om vi skal blokere udvekslingen. Pga. omfattende spørgsmål og bekymringer om Metas moderationspolitik har vi kigget nærmere på sagen, som har fået fornyet motivation tidligt i 2025, da Meta besluttede at:

- Droppe deres nuværende moderation og omtaler X-platformen (tidl Twitter) som et forbillede.
- Løsne op for reglerne om hadtale og dermed bevæge sig i retning (endnu længere) væk fra vores egne regler og AUP.

## Beslutningskriterier

Med henvisning til vores regel "at der findes en seriøs mistillid til ejerens evne til at moderere indhold i henhold til vores AUP eller modereringspolitik":

* Seneste udtalelser fra Mark Zuckerberg om fremtiden for moderation på Metas platforme i 2025 [1]
* Omfattende negligering af ansvar ved folkedrab og krænkelser af menneskerettigheder på Metas platforme, som beskrevet af Amnesty i 2023 [2] [3] [4]
* Overlagt negligering af ansvar for/medvirken til statslige aktøres manipulation og omfattende misinformationskampagner, som dokumenteret af flere whistleblowers [5] [6] [7]
* Metas censur af politisk indhold, hvor vi opfatter, at Meta ville censurere indhold delt af data.coops medlemmer, som beskrevet af Human Rights Watch i 2023 [8]

Med henvisning til "At der findes modstridende interesser mellem ejeren og data.coop":

* At Meta generelt arbejder for en centraliseret, kommerciel anvendelse og indsamling af brugerdata i strid med data.coops formål og principper.

Med henvisning til "At ejerens aktiviteter udgør en eksistentiel trussel for en eller flere af data.coops services":

* Der findes begrundet mistanke for at Threads deltager i Fødiverset enten for at lokke brugere over på deres platforme eller udgøre en dominerende magtfaktor. Flere medlemmer har udtrykt bekymring for "embrace, extend, extinguish"[9] scenarier.

Med henvisning til "At der er mistanke om, at ejeren uden samtykke kommer til at anvende medlemmernes AcitvityPub-data, indsamlet gennem fødereringen eller på anden vis, på en måde, der påkræver samtykke."

* Nuværende AI-teknologier trænes uden samtykke på åbent indhold. Fordi ActivityPub-protokollen ikke muliggør en klar definition for samtykke omkring anvendelse af data, og fordi vi allerede ved, at AIer trænes i modstrid til gældende lov om ophavsret, ser vi det også som sandsynligt, at data fra data.coops brugere kan overføres via ActivityPub til Threads og dermed f.eks. bruges til AI-træning eller anden privatlivskrænkende profilering af medlemmer.

## Beslutning

Bestyrelsen er enige om, at der samlet set er gode nok argumenter for at blokere for Metas ActivityPub-platforme, i første omgang konkret Threads.net. Nogle argumenter er nok i sig selv, og sammenlagt udgør de et klart billede af, at vi ikke ønsker:

* Vores medlemmers data udvekslet med Meta/Threads.
* Indhold fra Meta/Threads udvekslet via vores servere.

***Vi beslutter derfor at blokere al ActivityPub-baseret udveksling af data med Meta og Threads.net. Herunder også andre potentielle, fremtidige ActivityPub-baserede tjenester fra Meta.***


## Kilder


[1] https://www.dr.dk/nyheder/viden/teknologi/i-2008-var-zuckerberg-en-stor-undskyldning-det-er-han-langtfra-i-dag

[2] https://www.amnesty.org/en/latest/news/2023/08/myanmar-time-for-meta-to-pay-reparations-to-rohingya-for-role-in-ethnic-cleansing/

[3] https://www.amnesty.org/en/latest/news/2023/10/meta-failure-contributed-to-abuses-against-tigray-ethiopia/

[4] https://erinkissane.com/meta-in-myanmar-full-series

[5] https://www.theguardian.com/technology/2021/apr/12/facebook-fake-engagement-whistleblower-sophie-zhang

[6] https://en.wikipedia.org/wiki/Frances_Haugen

[7] https://en.wikipedia.org/wiki/Facebook%E2%80%93Cambridge_Analytica_data_scandal

[8] https://www.hrw.org/report/2023/12/21/metas-broken-promises/systemic-censorship-palestine-content-instagram-and

[9] https://en.wikipedia.org/wiki/Embrace,_extend,_and_extinguish
