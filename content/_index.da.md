---
title: Velkommen til data.coop
---

<div class="message">
  <p>2025-01-12: data.coop blokerer for threads.net: <a href="{{< ref "/blog/defederating-meta.da.md" >}}">Læs om beslutningen »</a></p>
</div>

data.coop er et kooperativ, som ejer og driver en digital infrastruktur for medlemmerne. Vores grundlæggende formål er at passe på medlemmernes data.

Vores kerneprincipper er:

<dl class="principles-list">
  <dt>
    <img src="/static/img/icons/no-surveillance.svg" />
    Privatlivsbeskyttelse
  </dt>
  <dd>
    Vi er fælles om at beskytte vores data. Vi deler dem ikke for profit. Dine data transmitteres krypteret på nettet.
  </dd>
  <dt>
    <img src="/static/img/icons/encrypted.svg" />
    Kryptering
  </dt>
  <dd>
    Vi tilbyder løsninger, der er sikre og grundigt deklarerede.
  </dd>
  <dt>
    <img src="/static/img/icons/decentralised.svg" />
    Decentralisering
  </dt>
  <dd>
    Vores services snakker gerne sammen med andre decentrale services på nettet.
  </dd>
  <dt>
    <img src="/static/img/icons/zero-knowledge.svg" />
    Zero-knowledge
  </dt>
  <dd>
    Når det er muligt, sørger vi for, at systemadministratorer rent teknisk ikke kan tilgå medlemmernes data.
  </dd>
</dl>

Ud fra de kerneprincipper vil vi med tiden udbyde onlinetjenester til medlemmerne. Hovedtanken er, at vi som udgangspunkt stoler mere på hinanden end på "de store" som f.eks. Google, Microsoft eller Facebook.

